// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('todo', ['ionic','ui.router'])
/**
 * The Projects factory handles saving and loading projects
 * from local storage, and also lets us save and load the
 * last active project index.
 */
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            //If in a folder, template/welcome.html
            templateUrl: 'templates/home.html',
            controller: 'TodoCtrl'
        })
        .state('project', {
            url : '/project',
            views: {
                project: {
                    templateUrl: 'templates/project.html'
                },
                controller: 'TodoCtrl'
            }
        })
    $urlRouterProvider.otherwise("/home");
}]);



app.factory('Projects', function () {
    return {
        all: function () {
            var projectString = window.localStorage['projects'];
            if (projectString) {
                return angular.fromJson(projectString);
            }
            return [];
        },
        save: function (projects) {
            window.localStorage['projects'] = angular.toJson(projects);
        },
        newProject: function (projectTitle) {
            // Add a new project

            return {
                title: projectTitle,
                id: this.generateUUID(),
                done: false,
                vDate: Date.now(),
                tasks: []
            };
        },

        generateUUID: function () {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        },

        getLastActiveIndex: function () {
            return parseInt(window.localStorage['lastActiveProject']) || 0;
        },
        setLastActiveIndex: function (index) {
            window.localStorage['lastActiveProject'] = index;
        }
    }
})


app.controller('TodoCtrl', function ($scope, $timeout, $ionicModal, Projects, $ionicSideMenuDelegate) {

    // A utility function for creating a new project
    // with the given projectTitle
    var createProject = function (projectTitle) {
        var newProject = Projects.newProject(projectTitle);
        $scope.projects.push(newProject);
        Projects.save($scope.projects);
        $scope.selectProject(newProject, $scope.projects.length - 1);
    }


    // Load or initialize projects
    $scope.projects = Projects.all();

    // Grab the last active, or the first project
    $scope.activeProject = $scope.projects[Projects.getLastActiveIndex()];

    // Called to create a new project
    $scope.newProject = function () {
        var projectTitle = prompt('Название задачи первого уровня');
        if (projectTitle) {
            createProject(projectTitle);
        }
    };

    // Called to select the given project
    $scope.selectProject = function (project, index) {
        $scope.activeProject = project;
        Projects.setLastActiveIndex(index);
        $ionicSideMenuDelegate.toggleLeft(false);
        $state.go('project');
    };

    // Create our modal
    $ionicModal.fromTemplateUrl('templates/new-task.html', function (modal) {
        $scope.taskModal = modal;
    }, {
        scope: $scope
    });

    // Create our modal for edit
    $ionicModal.fromTemplateUrl('templates/edit-task.html', function (modal) {
        $scope.taskModalEditTask = modal;
    }, {
        scope: $scope
    });

    // Create our modal for edit Project
    $ionicModal.fromTemplateUrl('templates/edit-project.html', function (modal) {
        $scope.taskModalEditProject = modal;
    }, {
        scope: $scope
    });

    $scope.taskDone = function (task) {
        Projects.save($scope.projects);
    };

    $scope.createTask = function (task) {
        if (!$scope.activeProject || !task) {
            return;
        }
        $scope.activeProject.tasks.push({
            id: Projects.generateUUID(),
            title: task.title,
            done: false,
            vDate: $scope.getDatetime()
        });
        $scope.taskModal.hide();

        // Inefficient, but save all the projects
        Projects.save($scope.projects);

        task.title = "";
    };

    $scope.newTask = function () {
        $scope.taskModal.show();
    };

    $scope.editTaskShow = function ($project, $task, $index) {
        $scope.taskToEdit = $task.title;
        $scope.curProj = $project;
        $scope.curTask = $task;
        $scope.taskModalEditTask.show();

    };

    $scope.getDatetime = function () {
        return (new Date).toLocaleFormat('%d.%m.%Y - %H:%M:%S');
        //return (new Date).toLocaleFormat("%A, %B %e, %Y");
    };

    $scope.editProjectShow = function ($project, $index) {
        $scope.projToEdit = $project.title;
        $scope.curProj = $project;
        $scope.taskModalEditProject.show();

    };

    $scope.editProject = function ($project, $title) {
        $project.title = $title;
        Projects.save($scope.projects);
        $scope.taskModalEditProject.hide();

    };

    $scope.closeEditProject = function () {
        $scope.taskModalEditProject.hide();
    };

    $scope.editTask = function ($project, $task, $title) {
        $task.title = $title;
        Projects.save($scope.projects);
        $scope.taskModalEditTask.hide();

    };

    $scope.closeEditTask = function () {
        $scope.taskModalEditTask.hide();
    };


    $scope.deleteTask = function (project, taskToDel, index) {
        project.tasks.splice(index, 1);
        Projects.save($scope.projects);
    };

    $scope.deleteProject = function (project, taskToDel, index) {
        $scope.projects.splice(index, 1);
        Projects.save($scope.projects);
    };

    $scope.closeNewTask = function () {
        $scope.taskModal.hide();
    };

    $scope.toggleProjects = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };


    // Try to create the first project, make sure to defer
    // this by using $timeout so everything is initialized
    // properly
    $timeout(function () {
        if ($scope.projects.length == 0) {
            while (true) {
                var projectTitle = prompt('Введите название вашей первой задачи:');
                if (projectTitle) {
                    createProject(projectTitle);
                    break;
                }
            }
        }
    });

});

